Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: randomcolor
Source: https://github.com/hansrodtang/randomcolor
Files-Excluded:
  Godeps/_workspace

Files: *
Copyright: 2014 Hans Rødtang
License: CC0 or BSD-2

Files: debian/*
Copyright: 2019 Thorsten Alteholz <debian@alteholz.de>
License: CC0 or BSD-2
Comment: Debian packaging is licensed under the same terms as upstream

License: CC0
 This software has been dedicated to the public domain under the CC0
 public domain dedication.
 .
 To the extent possible under law, the person who associated CC0 with
 libottery has waived all copyright and related or neighboring rights
 to libottery.
 .
 In Debian you can find a copy of the license text in
 /usr/share/common-licenses/CC0-1.0

License: BSD-2
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
